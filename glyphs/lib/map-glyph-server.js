const Router = require('router');


const findFonts = require('./find-fonts');
const getFontsPbf = require('./get-fonts-pbf');
const fontkit = require('fontkit');

const CACHE_MAX_AGE = process.env.MAP_GLYPH_SERVER_CACHE_MAX_AGE;

function find(req, res, next) {
  const {fontPath} = req;

  findFonts(fontPath, function (err, fonts) {
    if (err) {
      return next(err);
    }
    req.fonts = fonts;
    req.fontList = Object.keys(fonts);
    next();
  });
}

function sendFontsList(req, res) {
  const fontList = req.fontList.sort();
  res.setHeader('Content-Type', 'application/json');
  return res.end(JSON.stringify(fontList));
}

function sendFontsPbf(req, res) {
  const {fontPath} = req;
  let {fontstack, range} = req.params;

  fontstack = decodeURI(fontstack).split(',');

  getFontsPbf(fontPath, fontstack, range, req.fontList, function (err, pbf) {
    if (err) {
      let status = typeof err === 'number' ? err : 500;
      return res.sendStatus(status);
    }
    if (pbf.length === 0) {
      return res.sendStatus(204);
    }

    res.setHeader('Content-Type', 'application/x-protobuf');
    res.end(pbf);
  });
}

function headers(req, res, next) {
  if (CACHE_MAX_AGE) {
    res.setHeader('Cache-Control', `public, max-age=${CACHE_MAX_AGE}`);
  }
  next();
}

function getFont(req, res) {

  var url = require('url')
  var url_parts = url.parse(req.url, true)
  var params = url_parts.query

  if (params?.mode) {
    if (params.mode === "symbol") {
      const font = fontkit.openSync(`./glyphs/raw-symbols/${params.name}`)
      const charSet = font.characterSet

      let result = []

      try {
        for (let i = 0; i < charSet.length; i++) {
          const glyph = font.glyphForCodePoint(charSet[i])
          result.push({
            path: glyph.path.toSVG(),
            id: glyph.id,
            name: glyph.name,
            code_points: charSet[i],
            bbox: glyph.bbox,
            cbox: glyph.cbox
          })
        }
      } catch (e) {
        console.log(e)
      }

      res.setHeader('Content-Type', 'application/json')
      res.end(JSON.stringify(result))


    } else if (params.mode === "font") {
      const font = fontkit.openSync(`./glyphs/raw-fonts/${params.name}`)
      const charSet = font.characterSet

      let result = []

      try {
        for (let i = 0; i < charSet.length; i++) {
          const glyph = font.glyphForCodePoint(charSet[i])
          result.push({
            path: glyph.path.toSVG(),
            id: glyph.id,
            name: glyph.name,
            code_points: charSet[i],
            bbox: glyph.bbox,
            cbox: glyph.cbox
          })
        }
      } catch (e) {
        console.log(e)
      }

      res.setHeader('Content-Type', 'application/json')
      res.end(JSON.stringify(result))

    } else {
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify({error: "bad request"}));
    }
  } else {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({error: "bad request"}));
  }

}

module.exports = function (fontPath) {
  const router = new Router({
    strict: true,
    caseSensitive: true
  });

  router.use(function (req, res, next) {
    req.fontPath = fontPath;
    next();
  });

  router.get('/font/fonts/:fontstack/:range([\\d]+-[\\d]+).pbf', find, headers, sendFontsPbf);
  router.get('/font/fonts.json', find, sendFontsList);
  router.get('/font/font', getFont);
  return router;
};
