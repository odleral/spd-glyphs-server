FROM node:current

WORKDIR /app
COPY . .
RUN npm install glyphs/
WORKDIR /app/glyphs
RUN npm install cors
WORKDIR /app

EXPOSE 3060 3060
CMD ["node", "./glyphs"]